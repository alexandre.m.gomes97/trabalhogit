﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using Grpc.Net.Client;
using gRPCserver;
using Grpc.Core;
using gRPCserver.Models;
using gRPCcliente.Properties.Models;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace gRPCcliente.Controller
{
    class ControllerGame
    {
        private readonly Game.GameClient client;
        private static readonly HttpClient clientCredit = new HttpClient();

        public ControllerGame()
        {
            Program.V_Play.SendPlay += V_Play_SendPlayerPlay;
            Program.V_Stats.requestStats += V_Stats_requestStats;
            Program.V_Rank.requestRanks += V_Rank_requestRanks;
            Program.V_Credit.AskCredit += V_Credit_AskCredit;
            client = new gRPCserver.Game.GameClient(Program.channel);
            clientCredit.DefaultRequestHeaders.Accept.Clear();
            clientCredit.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

       

        private async void V_Credit_AskCredit(int value)
        {
            try
            {
                PostCredit postToSend = new PostCredit();
                postToSend.c_id = Program.idCreditNote;
                postToSend.value = value;
                var postToSendJson = JsonConvert.SerializeObject(postToSend);
                var stringContent = new StringContent(postToSendJson, UnicodeEncoding.UTF8, "application/json");
                var stringTask = await clientCredit.PostAsync("http://5c3424ca172c.ngrok.io/api/bank", stringContent);
                var responseMsg =  stringTask.Content.ReadAsStringAsync().Result;

                var result = JsonConvert.DeserializeObject<Response>(responseMsg);


                if (result.status == "error")
                    throw new Exception();

                System.Windows.Forms.Clipboard.SetText(result.ref_code);
                Program.V_Credit.MostraMensagem("Reference: " + result.ref_code + " (copied to clipboard)", "Success");
            }
            catch(Exception e)
            {
                Program.V_Credit.MostraMensagem("Server Error","ERROR");
            }
           

        }

        private async void V_Rank_requestRanks()
        {
            try
            {
                List<Player> players = new List<Player>();

            using (var call = client.PlayerRanks(new PlayerRanksRequest()))
            {
                   while(await call.ResponseStream.MoveNext())
                    {
                        var currentPlayer = call.ResponseStream.Current;
                        Player pp = new Player();
                        pp.Username = currentPlayer.Username;
                        pp.Wins = currentPlayer.Wins;
                        pp.Loses = currentPlayer.Loses;
                        pp.Draws = currentPlayer.Draws;
                        players.Add(pp);
                    }
               
            }
                Program.V_Rank.UpdateRanks(players);
            }
            catch(Exception e)
            {

            }
        }

        private void V_Stats_requestStats(int num)
        {
            try
            {
                var reply = client.PlayerStats(
                                    new PlayerStatsRequest { IdPlayer = Program.playerID });
               Program.V_Stats.LoadStats(reply.Username,reply.Email,reply.Wins,reply.Loses,reply.Draws);
            }
            catch (Exception e)
            {
                Program.V_Menu.errorMensagem("Some thing went wrong when connecting with the server: \n 1. Server is Offline. \n 2. You dont have a conection with the internet. \n 3. Server error.", "Connection Error");
            }
        }

        //jogadas Pedra-0,Papel-1,Tesoura-2
        private void V_Play_SendPlayerPlay(int num)
        {
            try
            {
                var reply = client.PlayServer(
                                    new PlayRequest { IdPlayer = Program.playerID, Play = num, Ref = Program.ativeCreditnote, IdC = Program.idCreditNote});
                Program.V_Play.ResponsePlay(reply.Play, reply.Message,reply.Money,reply.Canplay);
            }
            catch (Exception e)
            {
                Program.V_Play.errorMensagem("Some thing went wrong when connecting with the server: \n 1. Server is Offline. \n 2. You dont have a conection with the internet. \n 3. Server error.", "Connection Error");
            }
        }
    }
}
