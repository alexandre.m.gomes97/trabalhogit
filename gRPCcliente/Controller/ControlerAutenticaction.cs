﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using Grpc.Net.Client;
using gRPCserver;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace gRPCcliente.Controller
{
    class ControlerAutenticaction
    {
        private  Autenticaction.AutenticactionClient client;
        private static readonly HttpClient clientCredit = new HttpClient();
        public ControlerAutenticaction()
        {    
            Program.V_Home.TryConnect += V_Home_tryConnect1;
            Program.V_Register.signUpRequest += V_Register_signUpRequest;
            Program.V_ForgotPassord.recoverPasswordRequest += V_ForgotPassord_recoverPasswordRequest;
            Program.V_IP.Hello += V_IP_Hello;
            Program.V_creditRequest.validate += V_creditRequest_validate;
            client = new gRPCserver.Autenticaction.AutenticactionClient(Program.channel);
            clientCredit.DefaultRequestHeaders.Accept.Clear();
            clientCredit.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private void V_creditRequest_validate(int id_c, string result)
        {
            try
            {
                client = new gRPCserver.Autenticaction.AutenticactionClient(Program.channel);
                var reply = client.Creditnote(new credinoteRequest { Id = id_c, Ref = result });
                if(reply.Status == 1)
                {
                    Program.ativeCreditnote = result;
                    Program.currentMoney = reply.Value;
                    Program.V_creditRequest.Play();
                }
                else
                {
                    if (string.IsNullOrEmpty(result))
                        Program.V_IP.errorMensagem("Please insert the reference from the credite note", "Try again!");
                    else
                        Program.V_IP.errorMensagem(reply.Message, "Invalid credit note");
                }
            }
            catch(Exception e)
            {
               Program.V_IP.errorMensagem("Something went wront, please try again.","Try again!" );
            }
        }

        private void V_IP_Hello()
        {
            try
            {
                client = new gRPCserver.Autenticaction.AutenticactionClient(Program.channel);
                var reply = client.HelloServer(new HelloRequest { Message = "Try Connect" });
                Program.V_IP.Sucess("Connected","Success!");
            }
            catch(Exception e)
            {
                Program.V_IP.errorMensagem("Server not found. Try again!","Server Not found");
            }
        }

        private void V_ForgotPassord_recoverPasswordRequest(string email)
        {
            try
            {
                var reply = client.RecoverPassword(
                                  new RecoverPasswordRequest { Email = email });

                Program.V_ForgotPassord.recoverSucess(reply.Message, "Success");
            }
            catch (Exception e)
            {
                Program.V_ForgotPassord.errorMensagem("Some thing went wrong when connecting with the server: \n 1. Server is Offline. \n 2. You dont have a conection with the internet. \n 3. Server error.", "Connection Error");
            }
        }

        private void V_Register_signUpRequest(string username, string password, string email, string birthDate)
        {
       
            try
            {
                string title = "Error";
                var reply = client.Register(
                                  new RegisterRequest { Username = username, Password = password,Email = email, BirthDate = birthDate });
                if (reply.State == "1")
                    title = "Sucess";
                Program.V_Register.signUpSucess(reply.Message, title);
            }
            catch (Exception e)
            {
                Program.V_Register.errorMensagem("Some thing went wrong when connecting with the server: \n 1. Server is Offline. \n 2. You dont have a conection with the internet. \n 3. Server error.","Connection Error");
            }
        }

        private async void V_Home_tryConnect1(string username, string password)
        {//lalaa
            try
            {
                string title = "Error";
                var reply = client.SayHello(
                                  new AutenticactionRequest { Name = username, Password = password });

                if (reply.State == "1")
                {
                    title = "Sucess";
                    Program.playerID = reply.IdPlayer;
                    Program.playerName = reply.Playername;

                    string nameToSend = Program.playerName + "_" + Program.playerID.ToString();

                    var stringTask = await clientCredit.GetStringAsync("http://5c3424ca172c.ngrok.io/api/client/" + nameToSend);

                    var x = JObject.Parse(stringTask);
                    var token = x["id"];
                    int idCredit = token.ToObject<int>();
                    Program.idCreditNote = idCredit;
                   
                }
                Program.V_Home.MostraMensagem(reply.Message, title);

            }
            catch (Exception e)
            {
                Program.V_Home.errorMensagem("Some thing went wrong when connecting with the server: \n 1. Server is Offline. \n 2. You dont have a conection with the internet. \n 3. Server error.");
            }
        }

    }
}
