﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace gRPCcliente.Views
{
    public partial class ViewStats : Form
    {
        public event MetodosComUmInteiro requestStats;

        public ViewStats()
        {
            InitializeComponent();
        }

        public void LoadStats(string username, string email, int wins, int loses, int draws)
        {
            txbUsername.Text = username;
            txbEmail.Text = email;
            lbDraws.Text = draws.ToString();
            lbLoses.Text = loses.ToString();
            lbWins.Text = wins.ToString();
            float allGames = wins + loses + draws;
            float winRatio = (float)wins / allGames;
            lbWinRatio.Text = (winRatio * 100).ToString() + "%";
        }

        private void ViewStats_Load(object sender, EventArgs e)
        {
            requestStats(Program.playerID);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            closingMethod();
        }


        private void closingMethod()
        {
            Program.V_Stats.Close();
            Program.V_Menu.Show();
        }

        private void ViewRegister_FormClosing(object sender, FormClosedEventArgs e)
        {
            closingMethod();
        }
    }
}

