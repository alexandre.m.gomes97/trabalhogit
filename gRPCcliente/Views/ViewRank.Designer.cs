﻿namespace gRPCcliente.Views
{
    partial class ViewRank
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.lbFirst = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbFifthScore = new System.Windows.Forms.Label();
            this.lbFourthScore = new System.Windows.Forms.Label();
            this.lbThirdScore = new System.Windows.Forms.Label();
            this.lbSecondScore = new System.Windows.Forms.Label();
            this.lbFirstScore = new System.Windows.Forms.Label();
            this.lbFifith = new System.Windows.Forms.Label();
            this.lbFourth = new System.Windows.Forms.Label();
            this.lbThird = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbSecond = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button1.Location = new System.Drawing.Point(181, 248);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(77, 26);
            this.button1.TabIndex = 2;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbFirst
            // 
            this.lbFirst.AutoSize = true;
            this.lbFirst.Location = new System.Drawing.Point(75, 37);
            this.lbFirst.Name = "lbFirst";
            this.lbFirst.Size = new System.Drawing.Size(85, 15);
            this.lbFirst.TabIndex = 2;
            this.lbFirst.Text = "Does not exist";
            this.lbFirst.Click += new System.EventHandler(this.lbFirst_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(89, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "label8";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(89, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "label7";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Wins:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("SimSun", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(16, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 19);
            this.label7.TabIndex = 1;
            this.label7.Text = "1st:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.lbFifthScore);
            this.groupBox1.Controls.Add(this.lbFourthScore);
            this.groupBox1.Controls.Add(this.lbThirdScore);
            this.groupBox1.Controls.Add(this.lbSecondScore);
            this.groupBox1.Controls.Add(this.lbFirstScore);
            this.groupBox1.Controls.Add(this.lbFifith);
            this.groupBox1.Controls.Add(this.lbFourth);
            this.groupBox1.Controls.Add(this.lbThird);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbSecond);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lbFirst);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(419, 230);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ranks";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label10.Location = new System.Drawing.Point(228, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 15);
            this.label10.TabIndex = 2;
            this.label10.Text = "Score";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label9.Location = new System.Drawing.Point(75, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 15);
            this.label9.TabIndex = 2;
            this.label9.Text = "Username";
            // 
            // lbFifthScore
            // 
            this.lbFifthScore.AutoSize = true;
            this.lbFifthScore.Location = new System.Drawing.Point(169, 183);
            this.lbFifthScore.Name = "lbFifthScore";
            this.lbFifthScore.Size = new System.Drawing.Size(0, 15);
            this.lbFifthScore.TabIndex = 2;
            // 
            // lbFourthScore
            // 
            this.lbFourthScore.AutoSize = true;
            this.lbFourthScore.Location = new System.Drawing.Point(169, 145);
            this.lbFourthScore.Name = "lbFourthScore";
            this.lbFourthScore.Size = new System.Drawing.Size(0, 15);
            this.lbFourthScore.TabIndex = 2;
            // 
            // lbThirdScore
            // 
            this.lbThirdScore.AutoSize = true;
            this.lbThirdScore.Location = new System.Drawing.Point(169, 108);
            this.lbThirdScore.Name = "lbThirdScore";
            this.lbThirdScore.Size = new System.Drawing.Size(0, 15);
            this.lbThirdScore.TabIndex = 2;
            // 
            // lbSecondScore
            // 
            this.lbSecondScore.AutoSize = true;
            this.lbSecondScore.Location = new System.Drawing.Point(169, 73);
            this.lbSecondScore.Name = "lbSecondScore";
            this.lbSecondScore.Size = new System.Drawing.Size(0, 15);
            this.lbSecondScore.TabIndex = 2;
            // 
            // lbFirstScore
            // 
            this.lbFirstScore.AutoSize = true;
            this.lbFirstScore.Location = new System.Drawing.Point(169, 37);
            this.lbFirstScore.Name = "lbFirstScore";
            this.lbFirstScore.Size = new System.Drawing.Size(0, 15);
            this.lbFirstScore.TabIndex = 2;
            // 
            // lbFifith
            // 
            this.lbFifith.AutoSize = true;
            this.lbFifith.Location = new System.Drawing.Point(75, 183);
            this.lbFifith.Name = "lbFifith";
            this.lbFifith.Size = new System.Drawing.Size(85, 15);
            this.lbFifith.TabIndex = 2;
            this.lbFifith.Text = "Does not exist";
            // 
            // lbFourth
            // 
            this.lbFourth.AutoSize = true;
            this.lbFourth.Location = new System.Drawing.Point(75, 145);
            this.lbFourth.Name = "lbFourth";
            this.lbFourth.Size = new System.Drawing.Size(85, 15);
            this.lbFourth.TabIndex = 2;
            this.lbFourth.Text = "Does not exist";
            // 
            // lbThird
            // 
            this.lbThird.AutoSize = true;
            this.lbThird.Location = new System.Drawing.Point(75, 108);
            this.lbThird.Name = "lbThird";
            this.lbThird.Size = new System.Drawing.Size(85, 15);
            this.lbThird.TabIndex = 2;
            this.lbThird.Text = "Does not exist";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("SimSun", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(16, 179);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 19);
            this.label8.TabIndex = 1;
            this.label8.Text = "5th:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("SimSun", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(16, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 19);
            this.label6.TabIndex = 1;
            this.label6.Text = "4th:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("SimSun", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(16, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "3rd:";
            // 
            // lbSecond
            // 
            this.lbSecond.AutoSize = true;
            this.lbSecond.Location = new System.Drawing.Point(75, 73);
            this.lbSecond.Name = "lbSecond";
            this.lbSecond.Size = new System.Drawing.Size(85, 15);
            this.lbSecond.TabIndex = 2;
            this.lbSecond.Text = "Does not exist";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("SimSun", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(16, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "2nd:";
            // 
            // ViewRank
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 280);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ViewRank";
            this.Text = "Ranking";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ViewRank_FormClosing);
            this.Load += new System.EventHandler(this.ViewRank_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbFirst;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbFifith;
        private System.Windows.Forms.Label lbFourth;
        private System.Windows.Forms.Label lbThird;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbSecond;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbFifthScore;
        private System.Windows.Forms.Label lbFourthScore;
        private System.Windows.Forms.Label lbThirdScore;
        private System.Windows.Forms.Label lbSecondScore;
        private System.Windows.Forms.Label lbFirstScore;
        private System.Windows.Forms.Label label10;
    }
}