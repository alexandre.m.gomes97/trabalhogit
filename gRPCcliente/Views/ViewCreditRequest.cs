﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace gRPCcliente.Views
{
    public partial class ViewCreditRequest : Form
    {
        public event MetodosComUmInteiroUmString validate;

        public ViewCreditRequest()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            validate(Program.idCreditNote, txbCreditNote.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.V_creditRequest.Hide();
            Program.V_Menu.Show();
        }

        public void Play()
        {
            txbCreditNote.Text = "";
            Program.V_Menu.jogogo();
        }
    }
}
