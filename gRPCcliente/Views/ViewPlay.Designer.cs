﻿namespace gRPCcliente.Views
{
    partial class ViewPlay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxComputer = new System.Windows.Forms.PictureBox();
            this.pictureBoxPedra = new System.Windows.Forms.PictureBox();
            this.pictureBoxPapel = new System.Windows.Forms.PictureBox();
            this.pictureBoxTesoura = new System.Windows.Forms.PictureBox();
            this.btnContinuar = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.lbMessage = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbMoney = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComputer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPedra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPapel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTesoura)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxComputer
            // 
            this.pictureBoxComputer.Location = new System.Drawing.Point(173, 21);
            this.pictureBoxComputer.Name = "pictureBoxComputer";
            this.pictureBoxComputer.Size = new System.Drawing.Size(128, 123);
            this.pictureBoxComputer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxComputer.TabIndex = 0;
            this.pictureBoxComputer.TabStop = false;
            // 
            // pictureBoxPedra
            // 
            this.pictureBoxPedra.Location = new System.Drawing.Point(12, 315);
            this.pictureBoxPedra.Name = "pictureBoxPedra";
            this.pictureBoxPedra.Size = new System.Drawing.Size(123, 123);
            this.pictureBoxPedra.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxPedra.TabIndex = 0;
            this.pictureBoxPedra.TabStop = false;
            this.pictureBoxPedra.Click += new System.EventHandler(this.pictureBoxPedra_Click);
            // 
            // pictureBoxPapel
            // 
            this.pictureBoxPapel.Location = new System.Drawing.Point(173, 315);
            this.pictureBoxPapel.Name = "pictureBoxPapel";
            this.pictureBoxPapel.Size = new System.Drawing.Size(128, 123);
            this.pictureBoxPapel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxPapel.TabIndex = 0;
            this.pictureBoxPapel.TabStop = false;
            this.pictureBoxPapel.Click += new System.EventHandler(this.pictureBoxPapel_Click);
            // 
            // pictureBoxTesoura
            // 
            this.pictureBoxTesoura.Location = new System.Drawing.Point(334, 315);
            this.pictureBoxTesoura.Name = "pictureBoxTesoura";
            this.pictureBoxTesoura.Size = new System.Drawing.Size(128, 123);
            this.pictureBoxTesoura.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxTesoura.TabIndex = 0;
            this.pictureBoxTesoura.TabStop = false;
            this.pictureBoxTesoura.Click += new System.EventHandler(this.pictureBoxTesoura_Click);
            // 
            // btnContinuar
            // 
            this.btnContinuar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnContinuar.Location = new System.Drawing.Point(130, 238);
            this.btnContinuar.Name = "btnContinuar";
            this.btnContinuar.Size = new System.Drawing.Size(82, 26);
            this.btnContinuar.TabIndex = 2;
            this.btnContinuar.Text = "Play Again";
            this.btnContinuar.UseVisualStyleBackColor = true;
            this.btnContinuar.Click += new System.EventHandler(this.btnContinuar_Click);
            // 
            // btnSair
            // 
            this.btnSair.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnSair.Location = new System.Drawing.Point(256, 238);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(82, 26);
            this.btnSair.TabIndex = 2;
            this.btnSair.Text = "Exit";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // lbMessage
            // 
            this.lbMessage.AutoSize = true;
            this.lbMessage.Font = new System.Drawing.Font("Showcard Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbMessage.Location = new System.Drawing.Point(130, 187);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(208, 23);
            this.lbMessage.TabIndex = 3;
            this.lbMessage.Text = "Make your choice";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Money:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lbMoney
            // 
            this.lbMoney.AutoSize = true;
            this.lbMoney.Location = new System.Drawing.Point(65, 21);
            this.lbMoney.Name = "lbMoney";
            this.lbMoney.Size = new System.Drawing.Size(16, 15);
            this.lbMoney.TabIndex = 4;
            this.lbMoney.Text = "...";
            this.lbMoney.Click += new System.EventHandler(this.label1_Click);
            // 
            // ViewPlay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 450);
            this.ControlBox = false;
            this.Controls.Add(this.lbMoney);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbMessage);
            this.Controls.Add(this.btnSair);
            this.Controls.Add(this.btnContinuar);
            this.Controls.Add(this.pictureBoxTesoura);
            this.Controls.Add(this.pictureBoxPapel);
            this.Controls.Add(this.pictureBoxPedra);
            this.Controls.Add(this.pictureBoxComputer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ViewPlay";
            this.Text = "Playing...";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ViewPlay_FormClosed);
            this.Load += new System.EventHandler(this.ViewPlay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComputer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPedra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPapel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTesoura)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxComputer;
        private System.Windows.Forms.PictureBox pictureBoxPedra;
        private System.Windows.Forms.PictureBox pictureBoxPapel;
        private System.Windows.Forms.PictureBox pictureBoxTesoura;
        private System.Windows.Forms.Button btnContinuar;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Label lbMessage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbMoney;
    }
}