﻿namespace gRPCcliente.Views
{
    partial class ViewStats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbWinRatio = new System.Windows.Forms.Label();
            this.lbDraws = new System.Windows.Forms.Label();
            this.lbLoses = new System.Windows.Forms.Label();
            this.lbWins = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txbUsername = new System.Windows.Forms.TextBox();
            this.txbEmail = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Email:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "Wins:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "Loses:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Draws:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 15);
            this.label6.TabIndex = 1;
            this.label6.Text = "Win Ratio:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbWinRatio);
            this.groupBox1.Controls.Add(this.lbDraws);
            this.groupBox1.Controls.Add(this.lbLoses);
            this.groupBox1.Controls.Add(this.lbWins);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(30, 155);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(202, 142);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stats";
            // 
            // lbWinRatio
            // 
            this.lbWinRatio.AutoSize = true;
            this.lbWinRatio.Location = new System.Drawing.Point(89, 114);
            this.lbWinRatio.Name = "lbWinRatio";
            this.lbWinRatio.Size = new System.Drawing.Size(44, 15);
            this.lbWinRatio.TabIndex = 2;
            this.lbWinRatio.Text = "label10";
            // 
            // lbDraws
            // 
            this.lbDraws.AutoSize = true;
            this.lbDraws.Location = new System.Drawing.Point(89, 86);
            this.lbDraws.Name = "lbDraws";
            this.lbDraws.Size = new System.Drawing.Size(38, 15);
            this.lbDraws.TabIndex = 2;
            this.lbDraws.Text = "label9";
            // 
            // lbLoses
            // 
            this.lbLoses.AutoSize = true;
            this.lbLoses.Location = new System.Drawing.Point(89, 59);
            this.lbLoses.Name = "lbLoses";
            this.lbLoses.Size = new System.Drawing.Size(38, 15);
            this.lbLoses.TabIndex = 2;
            this.lbLoses.Text = "label8";
            // 
            // lbWins
            // 
            this.lbWins.AutoSize = true;
            this.lbWins.Location = new System.Drawing.Point(89, 31);
            this.lbWins.Name = "lbWins";
            this.lbWins.Size = new System.Drawing.Size(38, 15);
            this.lbWins.TabIndex = 2;
            this.lbWins.Text = "label7";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button1.Location = new System.Drawing.Point(89, 334);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(77, 26);
            this.button1.TabIndex = 2;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txbUsername
            // 
            this.txbUsername.Location = new System.Drawing.Point(104, 59);
            this.txbUsername.Name = "txbUsername";
            this.txbUsername.ReadOnly = true;
            this.txbUsername.Size = new System.Drawing.Size(149, 23);
            this.txbUsername.TabIndex = 3;
            // 
            // txbEmail
            // 
            this.txbEmail.Location = new System.Drawing.Point(104, 100);
            this.txbEmail.Name = "txbEmail";
            this.txbEmail.ReadOnly = true;
            this.txbEmail.Size = new System.Drawing.Size(149, 23);
            this.txbEmail.TabIndex = 3;
            // 
            // ViewStats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 409);
            this.ControlBox = false;
            this.Controls.Add(this.txbEmail);
            this.Controls.Add(this.txbUsername);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ViewStats";
            this.Text = "Profile";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ViewRegister_FormClosing);
            this.Load += new System.EventHandler(this.ViewStats_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txbUsername;
        private System.Windows.Forms.TextBox txbEmail;
        private System.Windows.Forms.Label lbWinRatio;
        private System.Windows.Forms.Label lbDraws;
        private System.Windows.Forms.Label lbLoses;
        private System.Windows.Forms.Label lbWins;
    }
}