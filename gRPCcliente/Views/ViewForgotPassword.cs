﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace gRPCcliente.Views
{
    public partial class ViewForgotPassword : Form
    {
        public event MetodosComUmaString recoverPasswordRequest;
        public ViewForgotPassword()
        {
            InitializeComponent();
        }

        private void btnRecover_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txbEmail.Text))
            {
                errorMensagem("You must insert your email", "Empty!");
            }
            else
            {
                recoverPasswordRequest(txbEmail.Text);
            }
            
        }

        public void recoverSucess(string msg, string title)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
            closingMethod();
        }

        public void errorMensagem(string msg, string title)
        {
            MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        private void closingMethod()
        {
            txbEmail.Clear();
            Program.V_ForgotPassord.Hide();
            Program.V_Home.Show();
        }
    }
}
