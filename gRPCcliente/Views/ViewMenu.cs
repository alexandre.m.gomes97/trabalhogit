﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace gRPCcliente.Views
{
    public partial class ViewMenu : Form
    {
        public event MetodosSemParametros GoPlay;
        public event MetodosSemParametros CreditNotes;
        public ViewMenu()
        {
            InitializeComponent();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            Program.V_Menu.Hide();
            Program.V_creditRequest.Show();
        }

        public void jogogo()
        {
            Program.V_creditRequest.Hide();
            Program.V_Play.Show();
            Program.V_Play.ReloadView();
        }

        private void ViewMenu_Load(object sender, EventArgs e)
        {
            label2.Text = "Welcome " + Program.playerName;
        }
        public void ReloadName()
        {
            label2.Text = "Welcome " + Program.playerName;
        }

        public void errorMensagem(string msg, string title)
        {
            MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnStatus_Click(object sender, EventArgs e)
        {
           Program.V_Menu.Hide();
            Program.V_Stats.ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            Program.playerID = -1;
            Program.V_Menu.Hide();
            Program.V_Home.Show();
        }

        private void btnRank_Click(object sender, EventArgs e)
        {
            Program.V_Rank.UpdateR();
            Program.V_Rank.Show();
        }

        private void ViewMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.playerID = -1;
            Program.V_Menu.Hide();
            Program.V_Home.Show();
        }

        private void btnCredit_Click(object sender, EventArgs e)
        {
            Program.V_Menu.Hide();
            Program.V_Credit.Show();
        }
    }
}
