﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using gRPCcliente;

namespace gRPCcliente.Views
{
    public partial class ViewRegister : Form
    {
        public event MetodosComQuatroString signUpRequest;

        public ViewRegister()
        {
            InitializeComponent();
        }

        private void ViewRegister_Load(object sender, EventArgs e)
        {
            Program.V_Home.Hide();
        }

        private void ViewRegister_FormClosing(Object sender, FormClosingEventArgs e)
        {
            closingMethod();
        }

        public void errorMensagem(string msg, string title)
        {
            MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            string emailPattner = @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

            if (string.IsNullOrEmpty(txbUsername.Text) || string.IsNullOrEmpty(txbPassword.Text) || string.IsNullOrEmpty(txbEmail.Text))
            {
                errorMensagem("All the camps must be filled!","Register error");
            }
            if(!Regex.IsMatch(txbEmail.Text,emailPattner))
                errorMensagem("Please insert a valid email!", "Register error");
            else
            {
                SHA512 sha512 = SHA512Managed.Create();
                byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(txbPassword.Text), 0, Encoding.UTF8.GetByteCount(txbPassword.Text));
                string passHash = Convert.ToBase64String(bytes);
                signUpRequest(txbUsername.Text, passHash, txbEmail.Text, dateTimePickerBirthday.Text);
            }
            
        }

        public void signUpSucess(string msg,string title)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            
            if(title == "Sucess")
            {
                MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
                closingMethod();
            }
            else
            {
                MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                closingMethod();
            }
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
           closingMethod();
        }

        private void closingMethod()
        {
            txbEmail.Clear();
            txbUsername.Clear();
            txbPassword.Clear();
            dateTimePickerBirthday.ResetText();
            Program.V_Register.Hide();
            Program.V_Home.Show();
        }
    }
}
