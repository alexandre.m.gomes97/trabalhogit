﻿using Grpc.Net.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace gRPCcliente.Views
{
    public partial class ViewIP : Form
    {
        public event MetodosSemParametros Hello;
        public ViewIP()
        {
            InitializeComponent();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txbIP.Text))
            {
                txbIP.Text = "https://" + txbIP.Text;
                Program.channel = GrpcChannel.ForAddress(txbIP.Text);
            }
            else
            {
                Program.channel = GrpcChannel.ForAddress("https://localhost:5001");
            }

            Hello();
        }

        public void errorMensagem(string msg, string title)
        {
            MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
            txbIP.Text = "";
        }
        public void Sucess(string msg, string title)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
            Program.V_Home.Show();
            Program.V_IP.Hide();
        }
    }
}
