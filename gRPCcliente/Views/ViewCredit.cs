﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace gRPCcliente.Views
{
    public partial class ViewCredit : Form
    {
        public event MetodosComUmInteiro AskCredit;
        public ViewCredit()
        {
            InitializeComponent();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(Math.Round(numericUpDown1.Value, 0));
            AskCredit(count);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Program.V_Credit.Hide();
            Program.V_Menu.Show();
        }

        private void ViewCredit_Load(object sender, EventArgs e)
        {
            numericUpDown1.Maximum = Decimal.MaxValue;
            numericUpDown1.Minimum = 100;
        }

        public void MostraMensagem(string msg, string title)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            if (title == "Success")
            {
                
                MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Program.V_Credit.Hide();
                Program.V_Menu.Show();
            }
            else
            {
                MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
