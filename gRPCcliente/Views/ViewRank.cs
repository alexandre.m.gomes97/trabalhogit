﻿using gRPCserver;
using gRPCserver.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gRPCcliente.Views
{
    public partial class ViewRank : Form
    {
        public event MetodosSemParametros requestRanks;
        public ViewRank()
        {
            InitializeComponent();
        }

        private void ViewRank_Load(object sender, EventArgs e)
        {
            requestRanks();
        }

        public void UpdateRanks(List<Player> players)
        {
            int i = 0;

            var itemToRemove = players.Single(x => x.Username == "admin");
            players.Remove(itemToRemove);

            if (players[i] != null) { lbFirst.Text = players[i].Username; lbFirstScore.Text = "Wins: " + players[i].Wins + " || Draws: " + players[i].Draws + " || Loses: " + players[i].Loses; } i++;
            if (players[i] != null) { lbSecond.Text = players[i].Username; lbSecondScore.Text = "Wins: " + players[i].Wins + " || Draws: " + players[i].Draws + " || Loses: " + players[i].Loses; } i++;
            if (players[i] != null) { lbThird.Text = players[i].Username; lbThirdScore.Text = "Wins: " + players[i].Wins + " || Draws: " + players[i].Draws + " || Loses: " + players[i].Loses; } i++;
            if (players[i] != null) { lbFourth.Text = players[i].Username; lbFourthScore.Text = "Wins: " + players[i].Wins + " || Draws: " + players[i].Draws + " || Loses: " + players[i].Loses; } i++;
            if (players[i] != null) { lbFifith.Text = players[i].Username; lbFifthScore.Text = "Wins: " + players[i].Wins + " || Draws: " + players[i].Draws + " || Loses: " + players[i].Loses; } i++;



        }
        public void UpdateR()
        {
            requestRanks();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            closingMethod();
        }

        private void lbFirst_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void closingMethod()
        {
            Program.V_Rank.Hide();
        }

        private void ViewRank_FormClosing(object sender, FormClosingEventArgs e)
        {
            closingMethod();
        }
    }
}
