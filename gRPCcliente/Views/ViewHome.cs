﻿using gRPCcliente.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gRPCcliente
{
    public partial class ViewHome : Form
    {
        public event MetodosComDuasString TryConnect;
        public ViewHome()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btn_Exit(object sender, EventArgs e)
        {
            MessageBox.Show("See you later!", "Bye!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            Application.Exit();
        }
        private void btn_Connect(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            SHA512 sha512 = SHA512Managed.Create();
            byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(txbPassword.Text), 0, Encoding.UTF8.GetByteCount(txbPassword.Text));
            string passHash = Convert.ToBase64String(bytes);
            TryConnect(txbUsername.Text, passHash);
        }

        private void btn_SignUp(object sender, EventArgs e)
        {
            Program.V_Register.ShowDialog();
        }

        public void MostraMensagem(string msg,string title)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            if (title == "Sucess")
            {
                MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Program.V_Home.Hide();
                Program.V_Menu.Show();
                Program.V_Menu.ReloadName();

                txbPassword.Text = "";
                txbUsername.Text = "";
            }
            else
            {
                MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void errorMensagem(string msg)
        {
            MessageBox.Show(msg, "Connection error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void lbForgetPassword_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Program.V_ForgotPassord.ShowDialog();
        }

        private void ViewHome_FormClosing(object sender, FormClosingEventArgs e)
        {
            txbPassword.Text = "";
            txbUsername.Text = "";
        }
    }
}
