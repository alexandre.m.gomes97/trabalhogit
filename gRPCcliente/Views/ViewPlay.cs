﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace gRPCcliente.Views
{
    public partial class ViewPlay : Form
    {
        public event MetodosComUmInteiro SendPlay;
        private bool clickImage = true;

        public ViewPlay()
        {
            InitializeComponent();
       
            lbMoney.Text = Program.currentMoney.ToString() +"$";
        }

        public void ResponsePlay(int play, string result, string money, int canplay)
        {
            clickImage = false;
            Int32.TryParse(money, out int Money);
            Program.currentMoney = Money;

            switch (play)
            {
                case 0:
                    pictureBoxComputer.Image = Properties.Resources.pedra;
                    break;
                case 1:
                    pictureBoxComputer.Image = Properties.Resources.tesoura;
                    break;
                case 2:
                    pictureBoxComputer.Image = Properties.Resources.papel;
                    break;
            }
            lbMessage.Text = "     Result:" + result;
            btnContinuar.Visible = true;
            btnSair.Visible = true;

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            ReloadView();
            clickImage = true;
            Program.V_Play.Hide();
            Program.V_Menu.Show();
        }

        private void pictureBoxPedra_Click(object sender, EventArgs e)
        {
            pictureBoxPapel.Image = Properties.Resources.pedra;
            pictureBoxPedra.Visible = false;
            pictureBoxTesoura.Visible = false;
            lbMessage.Text = "Wait for oponnent";
            SendPlay(0);
        }

        private void pictureBoxPapel_Click(object sender, EventArgs e)
        {
            if (clickImage)
            {
                pictureBoxPapel.Image = Properties.Resources.papel;
                pictureBoxPedra.Visible = false;
                pictureBoxTesoura.Visible = false;
                lbMessage.Text = "Wait for oponnent";
                SendPlay(2);
            }
        }

        private void pictureBoxTesoura_Click(object sender, EventArgs e)
        {
            pictureBoxPapel.Image = Properties.Resources.tesoura;
            pictureBoxPedra.Visible = false;
            pictureBoxTesoura.Visible = false;
            lbMessage.Text = "Wait for oponnent";
            SendPlay(1);
        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            ReloadView();
            clickImage = true;
            lbMessage.Text = "Make your choice";
        }
        public void errorMensagem(string msg, string title)
        {
            MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
            ReloadView();
        }

        public void ViewPlay_Load(object sender, EventArgs e)
        {
            ReloadView();
        }

        public void ReloadView()
        {
            btnContinuar.Visible = false;
            btnSair.Visible = false;
            pictureBoxPapel.Visible = true;
            pictureBoxTesoura.Visible = true;
            pictureBoxPedra.Visible = true;
            pictureBoxPapel.Image = Properties.Resources.papel;
            pictureBoxTesoura.Image = Properties.Resources.tesoura;
            pictureBoxPedra.Image = Properties.Resources.pedra;
            pictureBoxComputer.Image = Properties.Resources.padrao;
            if (Program.currentMoney <= 0)
            {
                Program.currentMoney = 0;
            }
            lbMoney.Text = Program.currentMoney.ToString() + "$";
            lbMessage.Text = "Make your choice";
           if(Program.currentMoney <= 0)
            {
                MessageBox.Show("Dont have enough money!", "Game Over", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Program.V_Play.Hide();
                Program.V_Menu.Show();
                Program.currentMoney = 0;
            }

        }

        private void ViewPlay_FormClosed(object sender, FormClosedEventArgs e)
        {
            ReloadView();
            clickImage = true;

            Program.V_Play.Hide();
            Program.V_Menu.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
