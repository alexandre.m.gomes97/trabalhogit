using Grpc.Net.Client;
using gRPCcliente.Controller;
using gRPCcliente.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gRPCcliente
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        public static ViewHome V_Home { get; private set; }
        public static ViewRegister V_Register { get; private set; }
        public static ViewForgotPassword  V_ForgotPassord { get; private set; }
        public static ViewMenu V_Menu { get; private set; }
        public static ViewStats V_Stats { get; private set; }
        public static ViewPlay V_Play { get; set; }
        public static ViewRank V_Rank { get; private set; }

        public static ViewCredit V_Credit { get; private set; }
        public static ViewCreditRequest V_creditRequest { get;  private set; }

        public static ViewIP V_IP { get; private set; }
        public static ControlerAutenticaction C_Connection  { get;  private set; }
        public static ControllerGame C_Game { get;  private set; }
        public static GrpcChannel channel { get;  set; }
        public static int playerID { get;  set; }
        public static string playerName { get;   set; }
        public static int idCreditNote { get; set; }

        public static int currentMoney { get; set; }


        public static string ativeCreditnote { get; set; }

        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ativeCreditnote = "0";
            currentMoney = 0;
            V_Home = new ViewHome();
            V_Register = new ViewRegister();
            V_ForgotPassord = new ViewForgotPassword();
            V_Menu = new ViewMenu();
            V_Stats = new ViewStats();
            V_Play = new ViewPlay();
            V_Rank = new ViewRank();
            V_IP = new ViewIP();
            V_Credit = new ViewCredit();
            V_creditRequest = new ViewCreditRequest();
            channel = GrpcChannel.ForAddress("https://localhost:5001");
            C_Connection = new ControlerAutenticaction();
            C_Game = new ControllerGame();
            Application.Run(V_IP);
        }
    }
}
