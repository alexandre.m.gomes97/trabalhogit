﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace gRPCcliente
{
    public delegate void MetodosComQuatroString(string username, string password, string email, string birthDate);
    public delegate void MetodosComUmaString(string ficheiro);
    public delegate void MetodosComDuasString(string ficheiro, string conteudo);
    public delegate void MetodosSemParametros();
    public delegate void MetodosComUmInteiro(int num);
    public delegate void MetodosComDoisInteiro(int num1, int num2);
    public delegate void MetodosComUmInteiroUmString(int play, string result);
    public delegate void MetodoParaStats(string username, string email, int wins, int draws, int loses);

}
