﻿using gRPCserver.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gRPCserver.Models;

namespace gRPCserver.Data
{
    public class grpcServerDbContext : DbContext
    {
        public grpcServerDbContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Player>().HasData(
                GetPlayers()
                );
        }

        public DbSet<Player> Players { get; set; }

        private static List<Player> GetPlayers()
        {
            List<Player> players = new List<Player>()
            {
                new Player()
                {
                   PlayerId = 1,
                   Username = "admin",
                   Password = "123",
                   Email = "ale1x_vpac123ity@hotmail.com",
                   Wins = 0,
                   Loses = 0,
                   Draws = 0,
                   BirthDate = DateTime.Now

                }
            };

            return players;
        }
    }
}
