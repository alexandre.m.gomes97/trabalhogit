using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Grpc.Core;
using gRPCserver.Data;
using gRPCserver.Migrations;
using gRPCserver.Models;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Logging;

namespace gRPCserver
{
    public class AutenticationrService : Autenticaction.AutenticactionBase
    {
        private readonly ILogger<AutenticationrService> _logger;
        private readonly grpcServerDbContext _context;
        private readonly HttpClient _client;

        public AutenticationrService(ILogger<AutenticationrService> logger, grpcServerDbContext context)
        {
            _logger = logger;
            _context = context;
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

        }

        public override Task<AutenticactionReply> SayHello(AutenticactionRequest request, ServerCallContext context)
        {
            Player pp = _context.Players.FirstOrDefault(x => x.Username == request.Name);
            string replyMessage = "";
            string playerName="";
            int replyState = 0;
            int idPlayer = -1;

            if (pp == null)
                replyMessage = "User not found, please register your self before log in!";
            else
            {
                if(pp.Password == request.Password)
                {
                    replyMessage = "Welcome " + request.Name + "!";
                    replyState = 1;
                    idPlayer = pp.PlayerId;
                    playerName = pp.Username;
                }
                else
                {
                    replyMessage = "Wrong password please try again, or recover it!";
                }
            }

            return Task.FromResult(new AutenticactionReply
            {
                Message = replyMessage,
                State = replyState.ToString(),
                IdPlayer = idPlayer,
                Playername = playerName
               
            });
        }

        public override Task<RegisterReply> Register(RegisterRequest request, ServerCallContext context)
        {
            string replyMessage = request.Username + " aldready registed!";
            int replyState = 0;

            if (_context.Players.FirstOrDefault(x => x.Email == request.Email) != null)
            {
                replyMessage = request.Email + " is aldready in use!";
            }

            if (_context.Players.FirstOrDefault(x => x.Username == request.Username) == null && _context.Players.FirstOrDefault(x=> x.Email == request.Email) == null)
            {
                var pp = new Player(){Username = request.Username, BirthDate = DateTime.Parse(request.BirthDate),
                    Password = request.Password, Draws = 0,
                    Email = request.Email, Loses = 0, Wins = 0 };
                _context.Add(pp);
                _context.SaveChanges();
                replyMessage = request.Username + " registed with sucess!";
                replyState = 1;
            }
          


            return Task.FromResult(new RegisterReply
            {
                Message = replyMessage,
                State = replyState.ToString()
            });
        }

        public override Task<RecoverPasswordReply> RecoverPassword(RecoverPasswordRequest request, ServerCallContext context)
        {
            try
            {
                if(_context.Players.Any(x =>x.Email == request.Email))
                {
                    var newPass = RandomString(5);
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                    var pp = _context.Players.FirstOrDefault(x => x.Email == request.Email);
                   
                    mail.From = new MailAddress("sdpratico2@gmail.com");
                    mail.To.Add(request.Email);
                    mail.Subject = "Recovery Pass";
                    mail.Body = "Hello " + pp.Username  +"!\nThis is the new Password: " +  newPass;
                  
                    SmtpServer.Port = 587;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("sdpratico2@gmail.com", "TrabalhoPratico_2");
                    SmtpServer.EnableSsl = true;

                    SHA512 sha512 = SHA512Managed.Create();
                    byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(newPass), 0, Encoding.UTF8.GetByteCount(newPass));
                    string passHash = Convert.ToBase64String(bytes);
                    SmtpServer.Send(mail);
                    pp.Password = passHash;
                    _context.Update(pp);
                    _context.SaveChanges();
                    
                }
                else
                {
                    return Task.FromResult(new RecoverPasswordReply
                    {
                        Message = "Email not registered!"
                    }) ;
                }
                
  
            }
            catch (Exception ex)
            {
                return Task.FromResult(new RecoverPasswordReply
                {
                    Message = "An Error ocurred"
                }); 
            }
            return Task.FromResult(new RecoverPasswordReply
            {
                Message = "The instructions to recover the password were sent to:" + request.Email
            });
        }

        private static Random random = new Random();
        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public override Task<HelloReply> HelloServer(HelloRequest request,ServerCallContext context)
        {
        
            return Task.FromResult(new HelloReply
            {
                Message = "Connected"
            });
        }

        public override async Task<credinoteReply> Creditnote(credinoteRequest request, ServerCallContext context)
        {
            var _status = -1;
            var _value = -99;
            var _message = "The credit note isnt valid.";
            try
            {
                var stringTask = await _client.GetStringAsync("http://5c3424ca172c.ngrok.io/api/bank/" + request.Ref + "/" + request.Id);
                var msg = System.Text.Json.JsonSerializer.Deserialize<Message>(stringTask);

                if (msg.status == "sucess")
                {
                    _status = 1;
                    _value = msg.value;
                    _message = msg.message;
                }
                else
                    _message = msg.message;
            }
            catch(Exception e)
            {
                return await Task.FromResult(new credinoteReply
                {
                    Message = "The bank server is offline, please try again later.",
                    Status = _status,
                    Value = _value
                });
            }
          


            return await Task.FromResult(new credinoteReply
            {
                Message = _message,
                Status = _status,
                Value = _value
            });
        }


    }
}
