﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using gRPCserver.Data;
using gRPCserver.Models;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace gRPCserver

{
    public class GameService : Game.GameBase
    {
        private static readonly HttpClient clientCredit = new HttpClient();
        private readonly ILogger<AutenticationrService> _logger;
        private readonly grpcServerDbContext _context;
        public GameService(ILogger<AutenticationrService> logger, grpcServerDbContext context)
        {
            _logger = logger;
            _context = context;
            clientCredit.DefaultRequestHeaders.Accept.Clear();
            clientCredit.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

        }
        //jogadas Pedra-0,Papel-1,Tesoura-2

        public override async Task<PlayReply> PlayServer(PlayRequest request, ServerCallContext context)
        {
            if (_context.Players.FirstOrDefault(x => x.PlayerId == request.IdPlayer) == null)
            {
                return await Task.FromResult(new PlayReply
                {
                    Play = -99,
                    Message = "Error"
                });
            }

            int jogadaServidor = -1;
            string Winner = "";
            Random random = new Random();

            int[,] resultsPossible = new int[3, 3]
            {
                { 0, 1, -1},
                { -1, 0,  1},
                { 1, -1,  0}
            };
            jogadaServidor = random.Next(0, 3);

            int resultado = resultsPossible[jogadaServidor, request.Play];

            switch (resultado)
            {
                case -1:
                    Winner = "Client Wins";
                    break;
                case 0:
                    Winner = "Draw";
                    break;
                case 1:
                    Winner = "Server Wins";
                    break;
            }
            var pp = _context.Players.FirstOrDefault(x => x.PlayerId == request.IdPlayer);

            if (Winner == "Server Wins")
                pp.Loses += 1;
            if (Winner == "Client Wins")
                pp.Wins += 1;
            if (Winner == "Draw")
                pp.Draws += 1;

            _context.Update(pp);
            _context.SaveChanges();

            try
            {
                var Uri = "http://5c3424ca172c.ngrok.io/api/bankS/" + request.Ref + '/' + request.IdC;

                var stringTask = await clientCredit.GetStringAsync(Uri);

                var x = JObject.Parse(stringTask);
                var token = x["value"];
                int values = token.ToObject<int>();

                int canplay = 1;
                if(values == 0)
                {
                    canplay = 0;
                }

                return await Task.FromResult(new PlayReply
                {
                    Play = jogadaServidor,
                    Message = Winner,
                    Money = values.ToString(),
                    Canplay = canplay
                });
            }
            catch(Exception e)
            {

            }



            return await Task.FromResult(new PlayReply
            {
                Play = jogadaServidor,
                Message = Winner,
                Money = "0",
                Canplay = 0
            });
        }

        public override Task<PlayerStatsReply> PlayerStats(PlayerStatsRequest request, ServerCallContext context)
        {
            Player pepino = _context.Players.FirstOrDefault(x => x.PlayerId == request.IdPlayer);
            if (pepino == null)
            {
                return Task.FromResult(new PlayerStatsReply
                {
                    Wins = -99,
                    Loses = -99,
                    Draws = -99
                });
            }

            return Task.FromResult(new PlayerStatsReply
            {
                Username = pepino.Username,
                Email = pepino.Email,
                Wins = pepino.Wins,
                Loses = pepino.Loses,
                Draws = pepino.Draws
            });
        }

        public override async Task PlayerRanks(PlayerRanksRequest request, IServerStreamWriter<PlayerModel> responseStream, ServerCallContext context)
        {
            List<PlayerModel> listaP = new List<PlayerModel>();
            var players = _context.Players.OrderByDescending(x=>x.Wins);

            foreach(var pp in players)
            {
                PlayerModel rankedplayer = new PlayerModel();
                rankedplayer.Username = pp.Username;
                rankedplayer.Wins = pp.Wins;
                rankedplayer.Loses = pp.Loses;
                rankedplayer.Draws = pp.Draws;
                listaP.Add(rankedplayer);
            }

            foreach(var returnPlayer in listaP)
            {
               await responseStream.WriteAsync(returnPlayer);
            }
        }
    }
}