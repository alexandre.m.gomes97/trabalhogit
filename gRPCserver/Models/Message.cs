﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gRPCserver.Models
{
    public class Message
    {
        public string status { get; set; }
        public string message { get; set; }
        public string ref_code { get; set; }
        public DateTime expirationDate { get; set; }
        public int value { get; set; } //
    } 
}
